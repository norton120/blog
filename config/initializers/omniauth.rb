if File.exists?("#{Rails.root}/config/initializers/velociblogger_setup.rb") 
  Rails.application.config.middleware.use OmniAuth::Builder do
    provider :google_oauth2, API_KEYS['google']['client_id'], API_KEYS['google']['client_secret'], 
    {
      scope: 'profile, email',
    }
  end  
end