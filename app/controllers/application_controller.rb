class ApplicationController < ActionController::Base
   
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :setup_required?
  
  helper_method :is_admin?

  def login
    unless is_admin?
      redirect_to '/auth/google_oauth2'
    end  	
  end  

  def to_bool(x)
    case x
      when false then false	
      when "false" then false
      when "0" then false
      when nil then false
      else true
    end	
  end

   def setup_required?
    if File.exist?("#{Rails.root}/config/initializers/velociblogger_setup.rb")
      set_blog
    else
      redirect_to "/setup/step_1" 
    end  
  end 

  def set_blog
    @blog = Blog.first
  end
  
  def is_admin?
    session[:authorized_user] == API_KEYS['google']['email']
  end

end
