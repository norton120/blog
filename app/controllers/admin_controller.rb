class AdminController < ApplicationController
  before_action :login

  def index
    @categories = Category.all
    @posts = Post.all
  end

end
