class SessionsController < ApplicationController
  def new
  end 

  def create
  	auth_hash = request.env['omniauth.auth']
    @@g_data = auth_hash[:info]
    if @@g_data["email"] == API_KEYS['google']['email']
      session[:authorized_user] = API_KEYS['google']['email']  
      redirect_to "/"
    else
      denied
    end

  end

  def destroy
  	session[:authorized_user] = nil  
    #either swap users or log out of google
    if params.has_key?("switch") 
      redirect_to "https://accounts.google.com/AddSession" and return
    else
      redirect_to "https://accounts.google.com/Logout" and return
    end  
  end
  
  def failure
    session[:authorized_user] = nil  
    redirect_to "https://accounts.google.com/AddSession" and return
  end

end

