class Setup
  attr_reader :google_client_id, :google_client_secret, :google_email_address, :disqus_shortname  

  def initialize(args={})
  	@google_client_id = args[:google_client_id]
    @google_client_secret = args[:google_client_secret]
    @google_email_address = args[:google_email_address]
    @disqus_shortname = args[:disqus_shortname]
  end

  def generate_setup_file
    file_content = "API_KEYS = {
    'google'=>{'client_id'=>'#{self.google_client_id}', 'client_secret'=>'#{self.google_client_secret}', 'email'=>'#{self.google_email_address}'},
    'disqus'=>{'shortname'=>'#{self.disqus_shortname}'}
    }"
    File.open("#{Rails.root}/config/initializers/velociblogger_setup.rb", "w+"){|f| f.write(file_content)}
  end


end