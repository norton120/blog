class CreateBlogs < ActiveRecord::Migration
  def change
    create_table :blogs do |t|
      t.string :title
      t.text :admin
      t.string :theme
      t.string :twitter
      t.string :facebook
      t.string :google_plus
      t.string :linkedin

      t.timestamps null: false
    end
  end
end
