class RenameBlogAdminToAbout < ActiveRecord::Migration
  def change
    rename_column :blogs, :admin, :about 
  end
end
