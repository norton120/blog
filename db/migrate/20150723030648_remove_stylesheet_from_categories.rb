class RemoveStylesheetFromCategories < ActiveRecord::Migration
  def change
  	remove_column :categories, :stylesheet
  end
end
