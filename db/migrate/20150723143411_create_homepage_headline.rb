class CreateHomepageHeadline < ActiveRecord::Migration
  def change
    add_column :blogs, :homepage_text, :string
  end
end
